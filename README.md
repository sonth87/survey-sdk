# Survey SDK

### Build this project
```
yarn build
npm build
```

### How to use

#### #1: import script

React

```
<script
  src="https://unpkg.com/react@18.2.0/umd/react.production.min.js"
  crossorigin="anonymous"
></script>
<script
  src="https://unpkg.com/react-dom@18.2.0/umd/react-dom.production.min.js"
  crossorigin="anonymous"
></script>
```

Babel standalone

```
<script
  src="https://unpkg.com/@babel/standalone/babel.min.js"
  crossorigin="anonymous"
></script>
```

Main app

```
<script
  data-plugins="transform-modules-umd"
  type="text/babel"
  data-presets="react"
  data-type="module"
  src="link-to-file/main.js"
  defer
></script>
```

#### #2: Defined root element with id="survey-box"

```
<div
  id="survey-box"
  data-nguon="VBI4SALES-NOTIFICATION"
  data-lhnv="CORONA"
  data-user_id=""
  data-...="any-enviroment-as-you-want"

  theme="blue"                                // blue | pink
  background="#F8F8F8"                        // undifined | any color
  maxWidth="1200px"                           // section max width
  title="Ghi nhận ý kiến khách hàng"          // section title
  autoTitle="true"                            // if true, get title from survey data ( by field : ten_chien_dich )
  titlePosition="center"                      // left | center | right
  image="https://i.imgur.com/T9s7BQL.png"     // left image
  successMessage=""                           // undefined | custom success massage
  autoSuccessMessage="true"                   // if true, get message success from survey data ( by field : loi_cam_on )
/>
```
