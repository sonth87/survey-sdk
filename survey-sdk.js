"use strict";

function SurveyApp(props) {
  return (
    <div>
      <h1>Hello {props.text}</h1>
      <Input />
    </div>
  );
}

const domNode = document.getElementById("survey-box");
const root = ReactDOM.createRoot(domNode);

const source = domNode.getAttribute("data-source");

root.render(<SurveyApp text="World" source={domNode} />);
