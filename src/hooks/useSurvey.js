import React, { useEffect, useState } from "react";
import { checkingSurveyAvaible } from "../apis/survey";

const useCheckingSurveyAvailable = (sParams) => {
  const [survey, setSurvey] = useState(null);

  const check = async () => {
    const data = await checkingSurveyAvaible(sParams);

    setSurvey(data?.data);
  };

  useEffect(() => {
    if (sParams && !survey) check();
  }, [sParams, survey]);

  return { survey };
};

export { useCheckingSurveyAvailable };
