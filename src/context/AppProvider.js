import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useCheckingSurveyAvailable } from "../hooks/useSurvey";
import {
  PropertiesMapping,
  QuestInPage,
  QuestionComponentMapping,
  validateCurrentPage,
} from "../utils/mapping";
import { submitSurvey } from "../apis/survey";

const AppContext = createContext({
  sParams: [],
  ansForm: {},
  step: 1,
  totalPage: 1,
  cauhoi: [],
  traloi: [],
  chiendich: {},
  logic: [],
  showPreStep: false,
  showNextStep: true,
  onClickPreStep: () => {},
  onClickNextStep: () => {},
  onSubmit: () => {},
  pages: {},
  listQuestInPage: [],
  userAnswer: () => {},
  message: "",
  submitSuccess: false,
  resetForm: () => {},
});

const today = new Date();

const AppProvider = ({ attrs = [], numQuestInPage = 1, children }) => {
  // User key
  const time = useMemo(() => `-${today.getTime()}`, [attrs]);

  // Data
  const sParams = useMemo(
    () =>
      [
        ...attrs.filter((p) => p && !p?.["user_id"]),
        { user_id: attrs?.find((p) => p?.["user_id"])?.["user_id"] + time },
      ].filter((p) => p),
    [attrs]
  );

  const [ansForm, setAnsForm] = useState({});
  const [step, setStep] = useState(1);
  const { survey } = useCheckingSurveyAvailable(sParams);
  const [showPreStep, setShowPreStep] = useState(false);
  const [showNextStep, setShowNextStep] = useState(true);
  const [message, setMessage] = useState("");
  const [submitSuccess, setSubmitSuccess] = useState(false);

  // capture user answer
  const userAnswer = useCallback(
    (id_cauhoi, tra_loi, type) => {
      let _form = { ...ansForm };
      if (type === "2") {
        const _tra_loi = ansForm?.[id_cauhoi.toString()];
        const idx = _tra_loi.findIndex((tl) => typeof tl === "object");

        // _form = {
        //   ...ansForm,
        //   [id_cauhoi.toString()]: _tra_loi,
        // };
      } else _form = { ...ansForm, [id_cauhoi.toString()]: tra_loi };

      setAnsForm(_form);
    },
    [ansForm]
  );

  // Question
  const cauhoi = survey?.cau_hoi;
  const traloi = survey?.cau_tra_loi;
  const chiendich = survey?.chien_dich;
  const logic = chiendich?.logic_hien_thi;

  // get question list
  const questions = useMemo(
    () =>
      cauhoi?.map((ch) => {
        const mapping = PropertiesMapping(ch, traloi, ansForm);

        return {
          id: ch.id,
          ...ch,
          traloi: mapping?.traloi,
          q: QuestionComponentMapping(ch.loai_cau_hoi, mapping, userAnswer),
        };
      }),
    [cauhoi, traloi, ansForm]
  );

  const pages = useMemo(
    () => QuestInPage(questions, numQuestInPage, logic),
    [questions, logic]
  );

  const listQuestInPage = useMemo(
    () => pages?.pages?.find((p) => p?.page === step)?.question,
    [pages, step]
  );

  const totalPage = useMemo(() => pages.pages.length, [pages]);

  useEffect(() => {
    if (pages.pages.length === 1) {
      setShowNextStep(false);
      setShowPreStep(false);
    }
  }, [pages]);

  const onClickPreStep = useCallback(() => {
    const _step = step - 1;

    if (_step >= 1) setStep(_step);
    if (_step === 1) setShowPreStep(false);
    setShowNextStep(true);
  }, [step]);

  const onClickNextStep = useCallback(() => {
    // validate current page
    const curPage = pages?.pages?.find((p) => p.page === step);
    const check = validateCurrentPage(curPage, ansForm);

    if (check) {
      setMessage("");
      const _step = step + 1;

      if (_step <= pages.pages.length) {
        setStep(_step);
        setShowPreStep(true);
      }

      if (_step === pages.pages.length) setShowNextStep(false);
    } else {
      setMessage("Vui lòng hoàn tất các câu hỏi bắt buộc trước khi tiếp tục.");
    }
  }, [step, pages, ansForm]);

  const onSubmit = useCallback(async () => {
    console.log(ansForm, traloi);
    // alert("gửi luôn đây...");
    // alert("mà quên chưa va ni đết...");

    const allParam = {};
    sParams
      ?.filter((p) => p)
      ?.forEach((p) => {
        const ent = Object.entries(p)[0];
        allParam[ent[0]] = ent[1];
      });

    const _answs = [];
    Object.entries(ansForm)?.forEach((a) => {
      const qid = a[0];
      const ans = a[1];
      const findCauhoiTemp = cauhoi?.find((ch) => ch.id.toString() == qid);

      // neu la mang
      if (typeof ans === "object") {
        ans?.forEach((b) => {
          const findAnsTemp = traloi?.find((t) => t.id.toString() == b);

          const _answObj = {
            id_cau_hoi: qid,
            id_cau_tl: b?.toString(),
            khac: 0,
            loai_cau_hoi: findCauhoiTemp?.loai_cau_hoi,
            tra_loi: ans,
            trang_thai: 1,
          };
          _answs.push(_answObj);
        });
      } else {
        const findAnsTemp = traloi?.find((t) => t.id_cau_hoi.toString() == qid);
        let idtl = ans;
        if (findCauhoiTemp?.loai_cau_hoi === 6) {
          idtl = findAnsTemp.id;
        }
        const _answObj = {
          id_cau_hoi: qid,
          id_cau_tl: idtl?.toString(),
          khac: 0,
          loai_cau_hoi: findCauhoiTemp?.loai_cau_hoi,
          tra_loi: ans,
          trang_thai: 1,
        };
        _answs.push(_answObj);
      }
    });
    console.log(_answs);
    var dataObj = {
      ...allParam,
      b_so_id_survey: null,
      answer: _answs,
    };

    const response = await submitSurvey(dataObj);

    if (response) setSubmitSuccess(true);
  }, [ansForm]);

  const resetForm = () => {
    setSubmitSuccess(false);
    setMessage("");
    setStep(1);
    setAnsForm({});

    setShowNextStep(pages.pages.length === 1 ? false : true);
    setShowPreStep(false);
  };

  return (
    <AppContext.Provider
      value={{
        sParams,
        ansForm,
        step,
        totalPage,
        cauhoi,
        traloi,
        chiendich,
        logic,
        pages,
        listQuestInPage,
        showPreStep,
        showNextStep,
        onClickPreStep,
        onClickNextStep,
        userAnswer,
        onSubmit,
        message,
        submitSuccess,
        resetForm,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default AppProvider;
const useApp = () => useContext(AppContext);
export { useApp };
