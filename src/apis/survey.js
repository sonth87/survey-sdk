const baseUrl = "https://surveytest.evbi.vn/survey/web";
// const svapi = "./sample.json";
const svapi = baseUrl + "/getData";
const smapi = baseUrl + "/sendData";

/**
 *
 * @param {*} params [{user_id}, {nguon}, {lhnv}]
 * @returns
 */
export const checkingSurveyAvaible = async (params) => {
  const inline = params
    ? params
        .filter((a) => a)
        .map((a) => Object.entries(a).map((b) => b.join("=")))
        .join("&")
    : "";

  const api = svapi + (inline ? "?" + inline : "");

  const response = await fetch(api, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });

  const data = await response.json();

  // return data?.data;
  return data;
};

function flat(res, key, val, pre = "") {
  const prefix = pre ? pre + `[${key}]` : key;
  return val && typeof val === "object"
    ? Object.keys(val).reduce(
        (prev, curr) => flat(prev, curr, val[curr], prefix),
        res
      )
    : Object.assign(res, { [prefix]: val });
}

export const submitSurvey = async (data) => {
  const fData = Object.keys(data).reduce(
    (prev, curr) => flat(prev, curr, data[curr]),
    {}
  );

  const response = await fetch(smapi, {
    method: "POST",
    headers: {
      dataType: "Json",
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: new URLSearchParams(fData),
  });

  return response;
};
