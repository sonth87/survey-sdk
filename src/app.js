import React from "react";
import styled from "styled-components";
import QuestList from "./components/QuestList";
import { useApp } from "./context/AppProvider";
import Button from "./components/button/button";
import SuccessIcon from "./components/icons/success";

const AppWrapper = styled.div`
  background-color: ${({ $bg }) => $bg || "transparent"};
`;

const AppStyled = styled.div`
  color: #596481;
  padding: 16px;

  @media (min-width: 720px) {
    padding: 40px 0;
  }
`;

const AppTitle = styled.div`
  font-size: 24px;
  line-height: 32px;
  font-weight: 500;
  color: #2e3a5b;
  text-align: ${({ $pos }) => $pos || "center"};
  margin-bottom: 24px;
`;

const MainContent = styled.div`
  font-family: "Roboto", "Open Sans";
  font-weight: 400;
  display: grid;
  grid-template-columns: repeat(12, minmax(0, 1fr));
  gap: 16px;
  background-color: #fff;
  border-radius: 12px;
  padding: 16px;
  max-width: ${({ $maxWidth }) => $maxWidth || "100%"};
  margin: 0 auto;

  @media (min-width: 720px) {
    padding: 40px;
  }
`;

const ImageWrapper = styled.div`
  display: none;

  @media (min-width: 720px) {
    display: flex;
    justify-content: center;
    align-items: start;
    grid-column: span 4;
  }

  @media (min-width: 960px) {
    grid-column: span 6 / span 6;
  }
`;

const ImageStyled = styled.img`
  max-width: 100%;
  object-fit: cover;
`;

const QuestListWrapper = styled.div`
  grid-column: span 12;

  @media (min-width: 720px) {
    grid-column: span 8;
  }

  @media (min-width: 960px) {
    grid-column: span 6 / span 6;
  }
`;

const SuccessStyled = styled.div`
  grid-column: span 12;
  display: flex;
  flex-direction: column;
  gap: 24px;
  align-items: center;
`;

const SuccessMessageStyled = styled.div`
  font-size: 20px;
  line-height: 28px;
  font-weight: 500;

  @media (min-width: 720px) {
    font-size: 24px;
    line-height: 32px;
  }
`;

const App = (props) => {
  const { cauhoi, submitSuccess, chiendich, resetForm } = useApp();

  return (
    <AppWrapper $bg={props?.background}>
      {cauhoi?.length && (
        <AppStyled>
          {(props?.title || props?.autoTitle) && (
            <AppTitle $pos={props?.titlePosition}>
              {props?.title?.length
                ? props?.title
                : props?.autoTitle
                ? chiendich?.ten_chien_dich
                : ""}
            </AppTitle>
          )}

          <MainContent $maxWidth={props?.maxWidth}>
            {!submitSuccess && (
              <>
                {props?.image && (
                  <ImageWrapper>
                    <ImageStyled src={props?.image}></ImageStyled>
                  </ImageWrapper>
                )}
                <QuestListWrapper>
                  <QuestList />
                </QuestListWrapper>
              </>
            )}

            {submitSuccess && (
              <SuccessStyled>
                <SuccessIcon color={props?.theme?.border} />
                <SuccessMessageStyled>
                  {props?.successMessage?.length
                    ? props?.successMessage
                    : props?.autoSuccessMessage
                    ? chiendich?.loi_cam_on
                    : "'"}
                </SuccessMessageStyled>
                <Button label="Gửi khảo sát khác" onClick={resetForm}></Button>
              </SuccessStyled>
            )}
          </MainContent>
        </AppStyled>
      )}
    </AppWrapper>
  );
};

export default App;
