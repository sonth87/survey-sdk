import React, { useEffect } from "react";
import ReactDOM from "react-dom/client";
import { ThemeProvider } from "styled-components";
import { findGetParameter } from "./utils/common";
import AppProvider from "./context/AppProvider";
import App from "./app";

const version = "1.0.1";

function SurveyApp(props) {
  useEffect(() => {
    const style = document.createElement("style");
    style.append(
      `@import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap')`
    );
    document.head.appendChild(style);

    console.info("sv-" + version);
  }, []);

  return (
    <ThemeProvider theme={props?.theme}>
      <AppProvider attrs={attrs}>
        <App {...props} />
      </AppProvider>
    </ThemeProvider>
  );
}

const domNode = document.getElementById("survey-box");
const root = ReactDOM.createRoot(domNode);

const attrs = domNode.getAttributeNames()?.map((aname) => {
  if (/^(data-)/.test(aname)) {
    const value = domNode.getAttribute(aname);

    return { [aname.replace("data-", "")]: value };
  }
});

// const themeStyle = findGetParameter("theme");
const themeStyle = domNode.getAttribute("theme"); // theme : pink, blue
const image = domNode.getAttribute("image");
const background = domNode.getAttribute("background");
const maxWidth = domNode.getAttribute("maxWidth");
const title = domNode.getAttribute("title");
const autoTitle = domNode.getAttribute("autoTitle");
const titlePosition = domNode.getAttribute("titlePosition");
const successMessage = domNode.getAttribute("successMessage");
const autoSuccessMessage = domNode.getAttribute("autoSuccessMessage");

const theme = () => {
  switch (themeStyle) {
    case "pink":
      return {
        border: "#FF61A3",
        bg: "#FFF7FA",
      };
    case "blue":
      return {
        border: "#0B7EFB",
        bg: "#E6F2FF",
      };
    default:
      return {
        border: "#FF61A3",
        bg: "#FFF7FA",
      };
  }
};

const tPos = () => {
  switch (titlePosition) {
    case "center":
      return "center";
    case "left":
      return "left";
    case "right":
      return "right";
    default:
      return "center";
  }
};

root.render(
  <SurveyApp
    attrs={attrs.filter((a) => a)}
    theme={theme()}
    background={background}
    maxWidth={maxWidth}
    image={image}
    title={title}
    autoTitle={autoTitle}
    titlePosition={tPos()}
    successMessage={successMessage}
    autoSuccessMessage={autoSuccessMessage}
  />
);
