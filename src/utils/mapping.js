import UnSupport from "../components/UnSupport";
import CheckboxGroup from "../components/checkbox/checkboxGroup";
import Input from "../components/input/input";
import RadioGroup from "../components/radio/radioGroup";
import Select from "../components/select/select";

export const QuestionTypeMapping = {
  ["1"]: "radio", // Nhiều lựa chọn(Một câu trả lời)
  ["2"]: "checkbox", // Nhiều Lựa chọn(Nhiều câu trả lời)
  ["3"]: "select", // Danh sách xổ xuống
  ["4"]: "star", // Hạng sao
  ["5"]: "matrix", // Lựa chọn ma trận
  ["6"]: "input", // Câu trả lời nhập tay
  ["7"]: "multi-select", // Multicombobox
  ["8"]: "true-false", // Like/Dislike
};

export const QuestionComponentMapping = (type, props, onChange) => {
  switch (type.toString()) {
    case "1":
      return (
        <RadioGroup
          {...props}
          onChange={(e) => {
            onChange(props.id, e.target.value);
          }}
        />
      );
    case "2":
      return (
        <CheckboxGroup
          {...props}
          onChange={(value) => {
            onChange(props.id, value);
          }}
        />
      );
    case "3":
      return (
        <Select
          {...props}
          onChange={(e) => {
            onChange(props.id, e.target.value);
          }}
        />
      );
    case "4":
      return <UnSupport {...props} />;
    case "5":
      return <UnSupport {...props} />;
    case "6":
      return (
        <Input
          {...props}
          onChange={(e) => {
            onChange(props.id, e.target.value);
          }}
        />
      );
    case "7":
      return <UnSupport {...props} />;
    case "8":
      return <UnSupport {...props} />;

    default:
      return null;
  }
};

/**
 * get properties by cau hoi & tra loi
 * @param {*} cauhoi
 * @param {*} traloi
 * @returns
 */
export const PropertiesMapping = (cauhoi, traloi, answered) => {
  const id_cauhoi = cauhoi.id;
  const thisAns = traloi.filter((tl) => tl.id_cau_hoi === id_cauhoi);
  const option = thisAns.map((tl) => ({
    value: tl.id,
    label: tl.nd_tloi,
  }));

  return {
    id: cauhoi.id,
    label: cauhoi.ten_cau_hoi,
    name: cauhoi.id,
    required: cauhoi.bat_buoc_tl.toString() === "1" ? true : false,
    option: option || undefined,
    value: answered?.[cauhoi.id],
    traloi: thisAns,
  };
};

/**
 * Sap xep cau hoi, dua vao list, chia theo page
 * @param {*} cauhoi
 * @param {*} numberOfQuestInStep
 * @param {*} logic
 * @returns
 * {
 *  pages: [
 *    { page: 1, question: [] },
 *    { page: 2, question: [] },
 *  ],
 *  dependOn: [
 *    {...logic, question: {id, q: Element}}
 *  ]
 * }
 */
export const QuestInPage = (cauhoi, numberOfQuestInStep = 1, logic) => {
  const result = { pages: [], dependOn: [] };
  let curPage = 1;

  cauhoi?.forEach((ch) => {
    const curQId = ch.id;
    // neu chua co page nay (theo current page) thi push vao 1 page moi
    if (result.pages.findIndex((r) => r.page === curPage) === -1)
      result.pages.push({ page: curPage, question: [], depend: [] });

    // lay index cua page trong mang
    let idx = result.pages.findIndex((r) => r.page === curPage);
    if (idx < 0) idx = result.pages.length - 1;

    const foundLogic = logic.find((cl) => cl.id_cau_hthi === curQId);

    if (foundLogic) {
      // Tim xem cau hoi nay co trong logic hien thi boi cau hoi khac khong
      // Neu co thi push sang dependOn
      result.dependOn.push({ ...foundLogic, question: ch });

      const foundPageIdx = result.pages.findIndex((p) =>
        p.question.find((q) => q.id === foundLogic.id_cau_hoi)
      );

      if (foundPageIdx > -1)
        result.pages[foundPageIdx].depend.push({ ...ch, logic: foundLogic });
    } else if (result.pages[idx].question.length < numberOfQuestInStep) {
      // Neu trong page chua du so cau hoi thi push them
      result.pages[idx].question.push(ch);
    } else if (result.pages[idx].question.length === numberOfQuestInStep) {
      // Neu page nay du roi thi tao page moi va push cau hoi
      curPage++;
      result.pages.push({ page: curPage, question: [ch], depend: [] });
    }
  });
  // console.log(result)
  return result;
};

export const validateCurrentPage = (cauhoi, traloi) => {
  let valid = true;

  // tim cac cau hoi bat buoc tra loi trong list quest
  cauhoi?.question?.forEach((ch) => {
    const qId = ch.id;
    const required = ch.bat_buoc_tl;

    if (required && (!traloi?.[qId] || !traloi?.[qId].length)) {
      valid = false;
    }
  });

  // tim cac cau hoi phu thuoc
  cauhoi?.depend?.forEach((ch) => {
    const qId = ch.id;
    const required = ch.bat_buoc_tl;
    const qCHId = ch?.logic?.id_cau_hoi; // id cau hoi dieu kien
    const qTLTd = ch?.logic?.id_cau_tl; // id cau tra loi

    // tim cau hoi y/c trong list cau da tra loi
    const match =
      traloi?.[qCHId]?.toString() == qTLTd.toString() ||
      traloi?.[qCHId]?.includes(qTLTd.toString());

    if (match && required && (!traloi?.[qId] || !traloi?.[qId].length)) {
      valid = false;
    }
  });

  return valid;
};

export const showRelatedQuest = (cauhoi, dependQuest, traloi) => {
  // tim xem co cau hoi phu thuoc nao voi id nhu tren ko
  const findLg = dependQuest.find((q) => q.id_cau_hoi === cauhoi.id);

  if (findLg) {
    const id_cauhienthi = findLg.id_cau_hthi; // id cau hoi dc hien thi
    const id_cautraloi = findLg.id_cau_tl; // id cau tra loi dieu kien

    const answed = traloi?.[cauhoi.id]; // cau user da tra loi theo id

    if (
      answed &&
      (id_cautraloi === answed || answed?.includes(id_cautraloi.toString()))
    ) {
      return findLg.question.q;
    }
  }
  return null;
};

export const showOtherAnswer = (cauhoi, ansForm, onChange) => {
  if (ansForm?.[cauhoi?.id]) {
    console.log(cauhoi, ansForm);
    const traloi = cauhoi?.traloi;

    // Tim cau hoi khac
    const loaiHai = traloi?.find((tl) => tl.loai == 2);

    if (loaiHai && ansForm?.[cauhoi?.id]?.includes(loaiHai?.id.toString())) {
      return (
        <Input
          placeholder="Vui lòng nêu cụ thể"
          onChange={(e) => {
            onChange(cauhoi?.id, e.target.value, "2");
          }}
        />
      );
    }
  }

  return null;
};
