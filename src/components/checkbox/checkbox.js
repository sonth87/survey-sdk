import React, { useEffect } from "react";
import styled from "styled-components";

const CheckboxWrapper = styled.div`
  display: block;
  border: solid 1px
    ${(props) => (props.$checked === true ? props.theme.border : "#dbe0e6")};
  background-color: ${(props) =>
    props.$checked === true ? props.theme.bg : "transparent"};
  padding: 8px 12px;
  border-radius: 6px;
`;

const Mark = styled.span`
  display: inline-block;
  position: relative;
  border: 2px solid #8592ab;
  width: 14px;
  height: 14px;
  left: 0;
  border-radius: 4px;
  margin-right: 14px;
  vertical-align: middle;
  box-sizing: content-box;
  &::after {
    content: "";
    display: block;
    width: 0;
    height: 0;
    background-color: transparent;
    opacity: 0;
    left: 0%;
    top: 0%;
    position: absolute;
    transition: all 110ms;
    box-sizing: content-box;
  }
`;

const Input = styled.input`
  position: absolute;
  visibility: hidden;
  display: none;
  &:checked + ${Mark} {
    border: solid 2px ${(props) => props.theme.border};
    background-color: ${(props) => props.theme.border};

    &::after {
      width: 8px;
      height: 4px;
      opacity: 1;
      left: 2px;
      top: 2.5px;
      border-color: #fff;
      border-style: solid;
      border-width: 2px 2px 0 0;
      transform: rotate(129deg);
    }
  }
`;

const LabelStyled = styled.label`
  display: flex;
  cursor: pointer;
  padding: 5px 10px 5px 0;
  position: relative;
  ${(props) =>
    props.disabled &&
    `
        cursor: not-allowed;
        opacity: 0.4;
    `}
`;

function Checkbox({ id, name, value, checked, label, onChange, disabled }) {
  return (
    <CheckboxWrapper $checked={checked}>
      <LabelStyled>
        <Input
          id={id}
          name={name}
          value={value}
          type="checkbox"
          onChange={onChange}
          disabled={disabled}
          checked={checked}
        />
        <Mark />
        {label}
      </LabelStyled>
    </CheckboxWrapper>
  );
}

export default Checkbox;
