import React, { useState } from "react";
import Checkbox from "./checkbox";
import Wrapper from "../wrapper";
import styled from "styled-components";

const ListStyled = styled.div`
  display: flex;
  flex-direction: column;
  gap: 12px;
`;

const CheckboxGroup = ({
  name,
  label,
  required,
  option,
  error,
  onChange,
  disabled,
  value,
}) => {
  const [checkList, setCheckList] = useState(value || []);

  const handleCheckEvent = (e) => {
    const state = e.target.checked;
    const ansId = e.target.value;

    const _newList = [...checkList];
    const idx = _newList.findIndex((i) => i === ansId);
    if (idx >= 0) _newList.splice(idx, 1);
    else _newList.push(ansId);

    setCheckList(_newList);
    onChange(_newList);
  };

  return (
    <Wrapper label={label} required={required} error={error}>
      <ListStyled>
        {option?.map((opt) => (
          <Checkbox
            name={name}
            value={opt.value}
            label={opt.label}
            key={name + opt.value}
            onChange={handleCheckEvent}
            disabled={disabled}
            checked={value?.includes(opt.value.toString()) || false}
          />
        ))}
      </ListStyled>
    </Wrapper>
  );
};

export default CheckboxGroup;
