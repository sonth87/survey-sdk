import React from "react";
import styled from "styled-components";

const WrapperStyled = styled.div`
  display: flex;
  flex-direction: column;
`;

const LabelStyled = styled.div`
  font-size: 16px;
  line-height: 24px;
  font-weight: 500;
  color: #2e3a5b;
  margin-bottom: 8px;
`;

const RequiredStar = styled.span`
  color: red;
`;

const ErrorMessage = styled.div`
  margin-top: 4px;
  color: red;
  font-size: 14px;
  line-height: 18px;
`;

const Wrapper = ({ label, required, error, children }) => {
  return (
    <WrapperStyled>
      <LabelStyled>
        {label} {required && <RequiredStar>*</RequiredStar>}
      </LabelStyled>
      {children}

      {error && <ErrorMessage>{error}</ErrorMessage>}
    </WrapperStyled>
  );
};

export default Wrapper;
