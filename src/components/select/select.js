import React, { useState } from "react";
import styled from "styled-components";
import Wrapper from "../wrapper";

const SelectWrapper = styled.div`
  display: block;
  border: solid 1px #dbe0e6;
  background-color: transparent;
  padding: 0;
  border-radius: 6px;
`;

const SelectStyled = styled.select`
  height: 56px;
  border: none;
  outline: none;
  padding: 16px;
  width: 100%;
  border-radius: 6px;
  background-image: url("data:image/svg+xml,<svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M18.5303 8.46967C18.8232 8.76256 18.8232 9.23744 18.5303 9.53033L12.5303 15.5303C12.2374 15.8232 11.7626 15.8232 11.4697 15.5303L5.46967 9.53033C5.17678 9.23744 5.17678 8.76256 5.46967 8.46967C5.76256 8.17678 6.23744 8.17678 6.53033 8.46967L12 13.9393L17.4697 8.46967C17.7626 8.17678 18.2374 8.17678 18.5303 8.46967Z' fill='rgb(133, 146, 171, 1)'/></svg>");
  background-position-y: 50%;
  background-position-x: calc(100% - 20px);
  background-repeat: no-repeat;
  background-size: 24px;
  -webkit-appearance: none;
  appearance: none;
  color: #596481;
  font-size: 16px;
  box-sizing: border-box;

  &::-ms-expand {
    display: none;
  }
`;

function Select({
  id,
  name,
  value,
  option,
  label,
  required,
  error,
  onChange,
  disabled,
}) {
  const [filteredOpt, setFilteredOpt] = useState(option);

  return (
    <Wrapper label={label} required={required} error={error}>
      <SelectWrapper>
        <SelectStyled
          id={id}
          name={name}
          onChange={onChange}
          disabled={disabled}
        >
          <option></option>
          {filteredOpt?.map((opt) => (
            <option value={opt.value} key={opt.value}>
              {opt.label}
            </option>
          ))}
        </SelectStyled>
      </SelectWrapper>
    </Wrapper>
  );
}

export default Select;
