import React from "react";
import styled from "styled-components";
import Button from "./button";
import { useApp } from "../../context/AppProvider";

const ButtonGroupStyled = styled.div`
  display: flex;
  justify-content: end;
  gap: 16px;
`;

const ButtonGroup = ({
  showBack,
  showNext,
  backFn,
  nextFn,
  backLabel,
  nextLabel,
}) => {
  const {
    cauhoi,
    showPreStep,
    showNextStep,
    onClickNextStep,
    onClickPreStep,
    onSubmit,
    step,
    totalPage,
  } = useApp();

  if(!cauhoi) return null;

  return (
    <ButtonGroupStyled>
      {showPreStep && (
        <Button
          label={backLabel || "Quay lại"}
          onClick={onClickPreStep}
          type={1}
        />
      )}
      {showNextStep && (
        <Button label={nextLabel || "Tiếp theo"} onClick={onClickNextStep} />
      )}

      {step === totalPage && (
        <Button label={"Gửi khảo sát"} onClick={onSubmit} />
      )}
    </ButtonGroupStyled>
  );
};

export default ButtonGroup;
