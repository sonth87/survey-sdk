import React from "react";
import styled from "styled-components";

const ButtonStled = styled.button`
  border: solid 1px
    ${(props) =>
      props.$btnType === "secondary" ? props.theme.bg : props.theme.border};
  background: ${(props) =>
    props.$btnType === "secondary" ? props.theme.bg : props.theme.border};
  border-radius: 8px;
  padding: 8px 20px 8px 20px;
  color: ${(props) =>
    props.$btnType === "secondary" ? props.theme.border : "#fff"};
  cursor: pointer;
  transition: 0.3s;
  font-size: 16px;
  line-height: 24px;
  font-weight: 500;
  box-sizing: content-box;

  &:hover {
    filter: ${(props) =>
      props.$btnType === "secondary" ? "opacity(0.7)" : "brightness(0.8)"};
  }
`;

const Button = ({ label, type, onClick }) => {
  return (
    <div>
      <ButtonStled $btnType={type ? "secondary" : "primary"} onClick={onClick}>
        {label}
      </ButtonStled>
    </div>
  );
};

export default Button;
