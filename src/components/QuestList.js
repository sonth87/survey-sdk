import React, { Fragment } from "react";
import styled from "styled-components";
import ButtonGroup from "./button/buttonGroup";
import { useApp } from "../context/AppProvider";
import { showOtherAnswer, showRelatedQuest } from "../utils/mapping";

const ListStyled = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  gap: 16px;
  width: 100%;
`;

const MessageStyled = styled.div`
  font-size: 14px;
  color: red;
  border: solid 1px red;
  background: #fff7fa;
  padding: 16px;
  border-radius: 8px;
`;

const StepStyled = styled.div`
  position: absolute;
  right: 0;
  top: 0;
`;

const QuestList = () => {
  const { step, listQuestInPage, message, ansForm, pages, userAnswer } =
    useApp();

  return (
    <ListStyled>
      {/* <StepStyled>{step}</StepStyled> */}
      {message && <MessageStyled>{message}</MessageStyled>}
      {listQuestInPage?.map((ch) => (
        <Fragment key={ch.id}>
          {ch.q}
          {showRelatedQuest(ch, pages?.dependOn, ansForm)}
          {/* {showOtherAnswer(ch, ansForm, userAnswer)} */}
        </Fragment>
      ))}
      <ButtonGroup />
    </ListStyled>
  );
};

export default QuestList;
