import React from "react";
import Wrapper from "../wrapper";
import Radio from "./radio";
import styled from "styled-components";

const ListStyled = styled.div`
  display: flex;
  flex-direction: column;
  gap: 12px;
`;

const RadioGroup = ({
  name,
  label,
  required,
  option,
  error,
  onChange,
  disabled,
  value,
}) => {
  return (
    <Wrapper label={label} required={required} error={error}>
      <ListStyled>
        {option?.map((opt) => (
          <Radio
            name={name}
            value={opt.value}
            label={opt.label}
            key={name + opt.value}
            onChange={onChange}
            disabled={disabled}
            checked={value?.includes(opt.value.toString()) || false}
          />
        ))}
      </ListStyled>
    </Wrapper>
  );
};

export default RadioGroup;
