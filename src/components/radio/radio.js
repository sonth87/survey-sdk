import React, { useEffect } from "react";
import styled from "styled-components";

const RadioWrapper = styled.div`
  display: block;
  border: solid 1px
    ${(props) => (props.$checked === true ? props.theme.border : "#dbe0e6")};
  background-color: ${(props) =>
    props.$checked === true ? props.theme.bg : "transparent"};
  padding: 8px 12px;
  border-radius: 6px;
`;

const LabelStyled = styled.label`
  display: flex;
  cursor: pointer;
  padding: 5px 10px 5px 0;
  position: relative;
  ${(props) =>
    props.disabled &&
    `
        cursor: not-allowed;
        opacity: 0.4;
    `}
`;

const Mark = styled.span`
  display: inline-block;
  position: relative;
  border: 2px solid #8592ab;
  width: 16px;
  height: 16px;
  left: 0;
  border-radius: 50%;
  margin-right: 14px;
  vertical-align: middle;
  box-sizing: content-box;

  &::after {
    content: "";
    display: block;
    width: 0;
    height: 0;
    border-radius: 50%;
    background-color: ${(props) => props.theme.border};
    opacity: 0;
    left: 50%;
    top: 50%;
    position: absolute;
    transition: all 110ms;
  }
`;

const Input = styled.input`
  position: absolute;
  visibility: hidden;
  display: none;
  &:checked + ${Mark} {
    border: solid 2px ${(props) => props.theme.border};

    &::after {
      width: 12px;
      height: 12px;
      opacity: 1;
      left: 2px;
      top: 2px;
    }
  }

  &:checked + ${LabelStyled} {
    font-weight: 500;
    color: #2e3a5b;
  }

  &:checked + ${RadioWrapper} {
    border-color: #ff61a3;
    background-color: #fff7fa;
  }
`;

function Radio({ name, value, checked, label, onChange, disabled }) {
  return (
    <RadioWrapper $checked={checked}>
      <LabelStyled>
        <Input
          name={name}
          value={value}
          type="radio"
          onChange={onChange}
          disabled={disabled}
          checked={checked}
        />
        <Mark />
        {label}
      </LabelStyled>
    </RadioWrapper>
  );
}

export default Radio;
