import React, { useEffect } from "react";
import styled from "styled-components";
import Wrapper from "../wrapper";

const InputStyled = styled.input`
  height: 56px;
  border-radius: 8px;
  border: solid 1px #dbe0e6;
  background: #fff;
  padding: 16px;
  color: #596481;
  font-size: 16px;
  width: auto;
  box-sizing: border-box;

  &:focus {
    border: solid 1px ${(props) => props.theme.border};
    background: ${(props) => props.theme.bg};
  }

  &:focus-visible {
    outline: solid 1px
      ${({ theme, $isError }) => ($isError ? "#FF61A3" : theme.border)};
    border: solid 1px
      ${({ theme, $isError }) => ($isError ? "#FF61A3" : theme.border)};
  }
`;

function Input({
  id,
  name,
  value,
  label,
  required,
  placeholder,
  error,
  onChange,
  disabled,
}) {
  return (
    <Wrapper label={label} required={required} error={error}>
      <InputStyled
        $isError={false}
        placeholder={placeholder}
        value={value || ""}
        name={name}
        id={id}
        onChange={onChange}
        disabled={disabled}
      />
    </Wrapper>
  );
}

export default Input;
